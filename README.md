![](http://i2.wp.com/agroeye.pl/wp-content/uploads/2015/05/AE_logo_17_logo-01-01.png)

### How to install ###
1. [Download](https://bitbucket.org/Progea/agroeye/raw/0523c707da764e7c3a5eb5caf0e43856bfa74b7d/windows_bins/v1.6/setup_agroeye_v1.6.exe) agroeye_setup.exe
2. Install

>**Supported versions**
>
> - Soon in this repository will appear the source code with compiliation and installation instructions.
> - Currently only installer 64-bit version for Windows OS is available.

### Technical guide ###

The technical guide can be found in the following link [https://agroeye.bitbucket.io/](https://agroeye.bitbucket.io/)


----------
[Agroeye's homepage](http://www.agroeye.pl)